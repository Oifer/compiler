﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.C
{
    public class Constants
    {
        public const string Equal = " = ";
        public const string While = "while";
        public const string Div = "/";
        public const string Minus = "-";
        public const string Plus = "+";
        public const string Rem = "%";
        public const string Mul = "*";
        public const string IsEqual = "==";
        public const string IsNotEqual = "!=";
        public const string Conjunction = "&&";
        public const string Exit = "return 0";
        public const string Standart = "std";
        public const string StreamOut = Standart + "::cout";
        public const string Write = StreamOut + ".write";
    }
}
