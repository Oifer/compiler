%using Compiler.Helpers
%using Compiler.Nodes
%namespace Compiler
%output=CompilerParser.cs 
%partial 
%sharetokens
%start list
%visibility internal

%YYSTYPE Node

%token LITERAL  
%token LETTER
%token PRINT
%token EVAL
%token GETTYPE
%token RESET
%token EXIT
%token HELP
%token EOL
%token MULTIPLE
%token DIVISION
%token REMAINDER
%token SUM
%token SUBTRACTION
%token EQUAL
%token NOTEQUAL
%token CONJUNCTION
%token SUBSTRING
%token ASSIGN
%token LOOP
%token MODECHANGE
%token BREAK
%token FOO

%%

list
	:   /*empty */
    |   list statement EOL
    |   list error EOL						{ yyerrok(); }
    ;

//���������� �� ���� �����������. ���������� ��� ���������
statement	
	: sequence 								{ this.DoStat($1); }
	;

//������������������ ����������. ���������� �� ����������
sequence	
	: action								{ $$ = MakeSequence(NodeTag.sequence, $1); }
	| sequence action 						{ $$ = MakeSequence(NodeTag.sequence, $1, $2); }
	;

//�������� ��� �����������. ���������� �� ����������
action
	: ASSIGN '(' LETTER expr ')'			{ $$ = MakeBinary(NodeTag.assign, $3, $4); }
	| EXIT									{ $$ = MakeLeaf(NodeTag.exit); }
	| FOO									{ $$ = MakeLeaf(NodeTag.foo); }
	| BREAK									{ $$ = MakeLeaf(NodeTag.myBreak); }
	| LITERAL								{ $$ = $1; }
	| EVAL expr								{ $$ = MakeUnary(NodeTag.display, $2); }
	| GETTYPE expr							{ $$ = MakeUnary(NodeTag.getType, $2); }
	| MODECHANGE LITERAL					{ $$ = MakeUnary(NodeTag.changeMode, $2); }
	| HELP									{ $$ = MakeLeaf(NodeTag.help); }
	| RESET									{ $$ = MakeLeaf(NodeTag.clear); }
	| PRINT									{ $$ = MakeLeaf(NodeTag.print); }
	| '(' expr ')' LOOP '(' sequence ')'	{ $$ = MakeBinary(NodeTag.loop, $2, $6); } 
	;

//�������� ��� ����������. ���������� �� ����������
expr	
	:  MULTIPLE '(' expr expr ')'			{ $$ = MakeBinary(NodeTag.mul, $3, $4); }
	|  DIVISION '(' expr expr ')'			{ $$ = MakeBinary(NodeTag.div, $3, $4); }
	|  REMAINDER '(' expr expr ')'			{ $$ = MakeBinary(NodeTag.rem, $3, $4); }
	|  SUM '(' expr expr ')'				{ $$ = MakeBinary(NodeTag.plus, $3, $4); }
	|  SUBTRACTION '(' expr expr ')'		{ $$ = MakeBinary(NodeTag.minus, $3, $4); }
	|  EQUAL '(' expr expr ')'				{ $$ = MakeBinary(NodeTag.equal, $3, $4); }
	|  NOTEQUAL '(' expr expr ')'			{ $$ = MakeBinary(NodeTag.notEqual, $3, $4); }
	|  CONJUNCTION '(' expr expr ')'		{ $$ = MakeBinary(NodeTag.conjunction, $3, $4); }
	|  SUBSTRING '(' expr expr expr ')'		{ $$ = MakeTernary(NodeTag.subString, $3, $4, $5);}
	|  LITERAL          					// $$ is automatically lexer.yylval
	|  LETTER								// $$ is automatically lexer.yylval
	|  '-' expr %prec UMINUS 				{ $$ = MakeUnary(NodeTag.negate, $2); }
	;

%%
