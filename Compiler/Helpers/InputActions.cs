﻿using System;
using System.Linq;

using Compiler.Exceptions;
using Compiler.Lex;
using Compiler.Nodes;

namespace Compiler.Helpers
{
    /// <summary>
    /// Выполнение действий, подаваемых на вход
    /// </summary>
    internal static class InputActions
    {
        /// <summary>
        /// Присвоение переменной <paramref name="lhs"/> значения <paramref name="rhs"/>
        /// </summary>
        public static void Assign(Node lhs, Node rhs, Parser p)
        {
            if (!(rhs is Leaf))
                rhs = new Leaf(rhs.Eval(p), new MyPoint(rhs.Point.GetTokenEnd()));
            
            var reg = p.regs.FirstOrDefault(myReg => myReg.Name == lhs.Name && myReg.Tag != NodeTag.name);
            if (reg != null) //если уже есть элемент с таким именем
            {
                var leftType = lhs.GetChildType(lhs, p);
                var rightType = rhs.GetChildType(rhs, p);
                
                if (leftType != MyType.None)
                    if (leftType != rightType)
                        throw new MyException("Несовпадение типов. ",
                            new MyInvalidDataExceptionType(rhs.Point.GetTokenEnd()));

                var regToRemove = p.regs.FirstOrDefault(myReg => myReg.Name == lhs.Name);
                if (regToRemove == null)
                    throw new VariableWasntDeclaredException(rhs.Point.GetTokenEnd());

                p.regs.Remove(regToRemove);

                rhs = new Leaf(rhs.Eval(p), rhs.Point);
            }

            rhs.Name = lhs.Name;
            p.regs.Add(rhs);
        }

        public static string GetHelp(int maxNameLength)
        {
            return
                @"Имена регистронезависимы, являются строками распознаются первые " + maxNameLength.ToString() + @" символов,
выражения могут использовать числа или переменные. Переменные могут быть пустыми или содержать деревья выражений. 
Литералы - тривиальный пример корректного дерева.
Команды разделяются переносом строки или ';'. Допустимы --
    > [help]                       Текущая команда.
    > [exit]                       Завершает программу, ровно как и ^C.
    > [print]                      Печатает все переменные и их значения.
    > [reset]                      Удаляет все переменные
    > [eval] x                     Вычисляет выражение
    > [mode] val                   Меняет режим работы: val = 0: вычисление, val = 1: генерация кода
    > [type] expr                  Выводит тип переменной
    > [eq]  ( x expression )       Записывает выражение в переменную x
    > [add] ( x y )      =  x + y  Сложение чисел или конкатенация строк
    > [sub] ( x y )      =  x - y
    > -x                           Отрицание вещественной или логической переменной
    > [mul] ( x y )      =  x * y
    > [div] ( x y )      =  x / y
    > [rem] ( x y )      =  x % y  Остаток от деления
    > [iseq]    ( x y )  =  is x == y
    > [isnoteq] ( x y )  =  is x != y
    > [con]     ( x y )  =  x конъюнкция y (только булевские)
    > [substr]  (x y z)  =  выделение подстроки из X, начиная с y-того элемента длиной z. x - строка, y, z - целые числа
    > ( cond ) [while] ( expr expr ... expr ) цикл: пока cond = true крутит тело цикла
    логические значения: [true], [false]
    строки выделяются кавычками " + '\"' + "..." + '\"';
        }

        /// <summary>
        /// Очистка значений переменных
        /// </summary>
        public static void Clear(Parser p)
        {
            p.regs.Clear();
        }

        /// <summary>
        /// Генерация строкового представления объявленных переменных и их значений
        /// </summary>
        public static string GetRegisters(Parser p)
        {
            string str = "";

            for (int i = 0; i < p.regs.Count; i++)
            {
                if (p.regs[i] != null)
                {
                    str += "\n";
                    str += p.regs[i].Name + " = " + p.regs[i].Eval(p);
                }
            }

            return str;
        }

        /// <summary>
        /// Завершение выполнения
        /// </summary>
        public static void Exit()
        {
            Environment.Exit(0);
        }

        /// <summary>
        /// Вычисление <paramref name="node"/> и выдача результата
        /// </summary>
        /// <param name="node">Узел</param>
        public static string Display(Node node, Parser p)
        {
            MyValue result = node.Eval(p);
            string line = "result: ";
            switch (result.Type)
            {
                case MyType.Double:
                    line += CommonMethods.DoubleToString(result.Double);
                    break;

                case MyType.String:
                    line += result.String;
                    break;

                case MyType.Bool:
                    line += result.Bool ? "true" : "false";
                    break;
            }

            return line;
        }
    }
}
