﻿namespace Compiler.Helpers
{
    /// <summary>
    /// Выполняемые операции
    /// </summary>
    internal enum NodeTag
    {
        error,          //stuff
        foo,
        literal,
        name,
        myBreak,
        conjunction,    //math
        div,
        equal,
        minus,
        mul,
        negate,
        notEqual,
        plus,
        rem,
        subString,
        assign,         //states
        changeMode,
        clear,
        display,
        exit,
        getType,
        help,
        loop,
        print,
        sequence,
    }
}
