﻿using System.Linq;

using Compiler.C;
using Compiler.Exceptions;
using Compiler.Helpers;

namespace Compiler.Nodes.Actions
{
    /// <summary>
    /// Набор операций, выполняемых бинарным узлом
    /// </summary>
    internal class BinaryActions : BaseNodeActions<Binary>
    {
        public BinaryActions() : base(MyActions)
        { }

        /// <summary>
        /// Операции, выполняемые бинарным узлом
        /// </summary>
        public static readonly MyAction<Binary>[] MyActions = 
        {
            new MyAction<Binary>(
                NodeTag.assign, 
                2, 
                (args) => true,
                (p, node) => MyType.None, 
                (p, node) =>
                {
                    if (node.rhs.Tag == NodeTag.name)
                        if (p.regs.All(reg => reg.Name != node.rhs.Name))
                            throw new VariableWasntDeclaredException(node.rhs.Point.GetTokenEnd());

                    p.Assign(node.lhs, node.rhs);
                    return new MyValue();
                },
                (p, node) => node.AssignCode(p)),
            
            new MyAction<Binary>(
                NodeTag.loop, 
                2, 
                (args) => true,
                (p, node) => MyType.None, 
                (p, node) =>
                {
                    node.DoLoop(node.lhs, node.rhs, p);
                    return new MyValue();
                },
                (p, node) =>
                {
                    string code = Constants.While + " (" + node.lhs.GenerateCode(p) + ")";
                    code += '\n' + "{" + '\n' + node.rhs.GenerateCode(p) + '\n' + "}";
                    return code;
                }),

            //////////////////////////////////////////////////

            new MyAction<Binary>(
                NodeTag.div, 
                new [] { MyType.Double, MyType.Double },
                MyType.Double, 
                (p, node) => new MyValue() { Double = node.lhs.Eval(p).Double / node.rhs.Eval(p).Double },
                (p, node) => CommonMethods.GenerateArithmeticOperation(p, node, 
                    Constants.Div)),

            new MyAction<Binary>(
                NodeTag.minus, 
                new [] { MyType.Double, MyType.Double },
                MyType.Double, 
                (p, node) => new MyValue() { Double = node.lhs.Eval(p).Double - node.rhs.Eval(p).Double },
                (p, node) => CommonMethods.GenerateArithmeticOperation(p, node, 
                    Constants.Minus)),

            new MyAction<Binary>(
                NodeTag.plus, 
                new [] { MyType.Double, MyType.Double },
                MyType.Double, 
                (p, node) => new MyValue() { Double = node.lhs.Eval(p).Double + node.rhs.Eval(p).Double},
                (p, node) => CommonMethods.GenerateArithmeticOperation(p, node, 
                    Constants.Plus)),
            
            new MyAction<Binary>(
                NodeTag.plus, 
                new [] { MyType.String, MyType.String },
                MyType.String, 
                (p, node) => new MyValue() { String = node.lhs.Eval(p).String + node.rhs.Eval(p).String },
                (p, node) => CommonMethods.GenerateArithmeticOperation(p, node, 
                    Constants.Plus)),
            
            new MyAction<Binary>(
                NodeTag.rem, 
                new [] { MyType.Double, MyType.Double },
                MyType.Double, 
                (p, node) => new MyValue() { Double = node.lhs.Eval(p).Double % node.rhs.Eval(p).Double },
                (p, node) => CommonMethods.GenerateArithmeticOperation(p, node, 
                    Constants.Rem)),
            
            new MyAction<Binary>(
                NodeTag.mul, 
                new [] { MyType.Double, MyType.Double },
                MyType.Double, 
                (p, node) => new MyValue() { Double = node.lhs.Eval(p).Double * node.rhs.Eval(p).Double },
                (p, node) => CommonMethods.GenerateArithmeticOperation(p, node, 
                    Constants.Mul)),

            new MyAction<Binary>(
                NodeTag.equal, 
                new [] { MyType.Double, MyType.Double },
                MyType.Bool, 
                (p, node) => new MyValue() { Bool = node.lhs.Eval(p).Double == node.rhs.Eval(p).Double },
                (p, node) => 
                    CommonMethods.GenerateArithmeticOperation(p, node, Constants.IsEqual)),

            new MyAction<Binary>(
                NodeTag.equal, 
                new [] { MyType.String, MyType.String },
                MyType.Bool, 
                (p, node) => new MyValue() { Bool = node.lhs.Eval(p).String == node.rhs.Eval(p).String },
                (p, node) => 
                    CommonMethods.GenerateArithmeticOperation(p, node, Constants.IsEqual)),

            new MyAction<Binary>(
                NodeTag.equal, 
                new [] { MyType.Bool, MyType.Bool },
                MyType.Bool, 
                (p, node) => new MyValue() { Bool = node.lhs.Eval(p).Bool == node.rhs.Eval(p).Bool },
                (p, node) => 
                    CommonMethods.GenerateArithmeticOperation(p, node, Constants.IsEqual)),

            new MyAction<Binary>(
                NodeTag.notEqual, 
                new [] { MyType.Double, MyType.Double },
                MyType.Bool, 
                (p, node) => new MyValue() { Bool = node.lhs.Eval(p).Double != node.rhs.Eval(p).Double },
                (p, node) => 
                    CommonMethods.GenerateArithmeticOperation(p, node, Constants.IsNotEqual)),

            new MyAction<Binary>(
                NodeTag.notEqual, 
                new [] { MyType.String, MyType.String },
                MyType.Bool, 
                (p, node) => new MyValue() { Bool = node.lhs.Eval(p).String != node.rhs.Eval(p).String },
                (p, node) => 
                    CommonMethods.GenerateArithmeticOperation(p, node, Constants.IsNotEqual)),

            new MyAction<Binary>(
                NodeTag.notEqual, 
                new [] { MyType.Bool, MyType.Bool },
                MyType.Bool, 
                (p, node) => new MyValue() { Bool = node.lhs.Eval(p).Bool != node.rhs.Eval(p).Bool },
                (p, node) => 
                    CommonMethods.GenerateArithmeticOperation(p, node, Constants.IsNotEqual)),

            new MyAction<Binary>(
                NodeTag.conjunction, 
                new [] { MyType.Bool, MyType.Bool},
                MyType.Bool, 
                (p, node) => new MyValue() { Bool = node.lhs.Eval(p).Bool && node.rhs.Eval(p).Bool },
                (p, node) => 
                    CommonMethods.GenerateArithmeticOperation(p, node, Constants.Conjunction)),
        };
    }
}
